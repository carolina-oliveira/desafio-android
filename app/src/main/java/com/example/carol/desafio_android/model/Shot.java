package com.example.carol.desafio_android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Shot implements Parcelable {

    @SerializedName("id")
    private long id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("images")
    private Images images;

    @SerializedName("views_count")
    private Integer viewsCount;

    @SerializedName("likes_count")
    private Integer likesCount;

    @SerializedName("comments_count")
    private Integer commentsCount;

    @SerializedName("attachments_count")
    private Integer attachmentsCount;

    @SerializedName("user")
    private User user;

    public Shot(long id, String title, String description, Images images, Integer viewsCount, Integer likesCount, Integer commentsCount, Integer attachmentsCount, User user) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.images = images;
        this.viewsCount = viewsCount;
        this.likesCount = likesCount;
        this.commentsCount = commentsCount;
        this.attachmentsCount = attachmentsCount;
        this.user = user;
    }

    protected Shot(Parcel in) {
        id = in.readLong();
        title = in.readString();
        description = in.readString();
        images = in.readParcelable(Images.class.getClassLoader());
        if (in.readByte() == 0) {
            viewsCount = null;
        } else {
            viewsCount = in.readInt();
        }
        if (in.readByte() == 0) {
            likesCount = null;
        } else {
            likesCount = in.readInt();
        }
        if (in.readByte() == 0) {
            commentsCount = null;
        } else {
            commentsCount = in.readInt();
        }
        if (in.readByte() == 0) {
            attachmentsCount = null;
        } else {
            attachmentsCount = in.readInt();
        }
        user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Shot> CREATOR = new Creator<Shot>() {
        @Override
        public Shot createFromParcel(Parcel in) {
            return new Shot(in);
        }

        @Override
        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Integer getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(Integer viewsCount) {
        this.viewsCount = viewsCount;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Integer getAttachmentsCount() {
        return attachmentsCount;
    }

    public void setAttachmentsCount(Integer attachmentsCount) {
        this.attachmentsCount = attachmentsCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeParcelable(images, i);
        if (viewsCount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(viewsCount);
        }
        if (likesCount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(likesCount);
        }
        if (commentsCount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(commentsCount);
        }
        if (attachmentsCount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(attachmentsCount);
        }
        parcel.writeParcelable(user, i);
    }
}