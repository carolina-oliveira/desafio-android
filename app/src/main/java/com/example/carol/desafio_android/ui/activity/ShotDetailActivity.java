package com.example.carol.desafio_android.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.carol.desafio_android.R;
import com.example.carol.desafio_android.adapter.ShotDetailAdapter;
import com.example.carol.desafio_android.controller.ShotDetailController;
import com.example.carol.desafio_android.model.Shot;

public class ShotDetailActivity extends AppCompatActivity implements CallbackActivity<Shot> {
    private ShotDetailController shotDetailController = new ShotDetailController();
    private ShotDetailAdapter adapter = new ShotDetailAdapter(this);

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        Intent intent = getIntent();
        long id = intent.getLongExtra(ShotDetailAdapter.SHOT_ID, 0);
        shotDetailController.makeShotDetailCall(id, this);
    }

    @Override
    public void onCreateComplete(Shot model) {
        ShotDetailAdapter adapter;

        adapter = new ShotDetailAdapter(this);
        recyclerView.setAdapter(adapter);

        adapter.setShotItems(model);
        adapter.notifyDataSetChanged();
    }
}
