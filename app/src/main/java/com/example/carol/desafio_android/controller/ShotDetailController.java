package com.example.carol.desafio_android.controller;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.carol.desafio_android.R;
import com.example.carol.desafio_android.api.CustomApplication;
import com.example.carol.desafio_android.model.Shot;
import com.example.carol.desafio_android.ui.activity.CallbackActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShotDetailController extends AppCompatActivity {
    private String LOG_TAG = ShotDetailController.class.getSimpleName();

    public void makeShotDetailCall(long id, final CallbackActivity<Shot> callbackActivity) {
        final CustomApplication application = (CustomApplication) getApplication();
        application.getApi()
                .getShotItem(id)
                .enqueue(new Callback<Shot>() {
                    @Override
                    public void onResponse(Call<Shot> call, Response<Shot> response) {
                        if (response.isSuccessful()) {
                            callbackActivity.onCreateComplete(response.body());

                        } else {
                            Toast.makeText(getApplicationContext(), R.string.retrieve_shot_detail_error_message, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, "Error: ");
                        }
                    }

                    @Override
                    public void onFailure(Call<Shot> call, Throwable t) {
                        Toast.makeText(ShotDetailController.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        Log.e("MAIN", "Error", t);
                    }
                });
    }
}