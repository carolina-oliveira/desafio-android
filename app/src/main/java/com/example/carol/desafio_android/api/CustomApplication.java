package com.example.carol.desafio_android.api;

import android.app.Application;

import com.example.carol.desafio_android.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomApplication extends Application {
    private static RetrofitAPI retrofitApi;
    private static final String BASE_URL = "https://api.dribbble.com/v1/";

    @Override
    public void onCreate() {
        super.onCreate();

        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG
                ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);

        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitApi = retrofit.create(RetrofitAPI.class);
    }

    public static RetrofitAPI getApi() {
        return retrofitApi;
    }
}