package com.example.carol.desafio_android.ui.activity;

public interface CallbackActivity<T> {
    void onCreateComplete(T model);
}