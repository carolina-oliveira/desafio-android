package com.example.carol.desafio_android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carol.desafio_android.R;
import com.example.carol.desafio_android.model.Shot;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShotDetailAdapter extends RecyclerView.Adapter<ShotDetailAdapter.ViewHolder> {
    public static String SHOT_ID = "id";

    private Shot shotItem;
    private Context context;

    public ShotDetailAdapter(Context context) {
        this.context = context;
    }

    public void setShotItems(Shot shotItem) {
        this.shotItem = shotItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_shot_detail, parent, false);
        return new ShotDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.shot = shotItem;
        holder.detailTitle.setText(shotItem.getTitle());
        holder.detailAuthor.setText(String.format(context.getString(R.string.written_by_txt), shotItem.getUser().getName()));
        Picasso.with(context)
                .load(shotItem.getImages().getHidpi())
                .into(holder.detailImage);
        holder.detailDesc.setText(Html.fromHtml(shotItem.getDescription()));
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Shot shot;

        @BindView(R.id.shot_detail_title)
        TextView detailTitle;

        @BindView(R.id.shot_detail_author)
        TextView detailAuthor;

        @BindView(R.id.shot_detail_image)
        ImageView detailImage;

        @BindView(R.id.shot_detail_desc)
        TextView detailDesc;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}