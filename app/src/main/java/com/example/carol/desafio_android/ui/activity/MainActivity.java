package com.example.carol.desafio_android.ui.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.carol.desafio_android.R;
import com.example.carol.desafio_android.adapter.ShotAdapter;
import com.example.carol.desafio_android.controller.ShotController;
import com.example.carol.desafio_android.model.Shot;
import com.example.carol.desafio_android.utils.EndlessRecyclerViewScrollListener;

import java.util.List;

public class MainActivity extends AppCompatActivity implements CallbackActivity<List<Shot>> {
    private ShotAdapter adapter = new ShotAdapter(this);
    private ShotController controller = new ShotController();

    private SwipeRefreshLayout swipeContainer;
    private RecyclerView recyclerView;
    String sort = "views";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                adapter.cleanList();
                loadData(1);
            }
        });

        adapter = new ShotAdapter(this);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadData(page);
                adapter.notifyDataSetChanged();
                resetState();
            }
        });

        loadData(1);
    }

    @Override
    public void onCreateComplete(List<Shot> model) {
        if (model != null) {
            adapter.setShotList(model, sort);
            adapter.notifyDataSetChanged();
        }
        swipeContainer.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_shot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String newSort = "";
        switch (id) {
            case R.id.sort_recent:
                newSort = "recent";
                break;
            case R.id.sort_most_viewed:
                newSort = "views";
                break;

            case R.id.sort_most_commented:
                newSort = "comments";
                break;
        }
        if (newSort != "") {
            sort = newSort;
            controller.makeShotListCall(sort, 1, this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadData(int offset) {
        controller.makeShotListCall(sort, offset, this);
    }
}