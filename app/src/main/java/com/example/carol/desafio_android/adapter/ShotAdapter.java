package com.example.carol.desafio_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carol.desafio_android.R;
import com.example.carol.desafio_android.model.Shot;
import com.example.carol.desafio_android.ui.activity.ShotDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.carol.desafio_android.adapter.ShotDetailAdapter.SHOT_ID;

public class ShotAdapter  extends RecyclerView.Adapter<ShotAdapter.ViewHolder> {
    private List<Shot> shotList = new ArrayList<>();
    private Context context;
    private String sort;

    public ShotAdapter(Context context) {
        this.context = context;
    }

    public void setShotList(List<Shot> shotList, String sort) {
        if (this.sort != sort)
            this.shotList = shotList;
        else
            this.shotList.addAll(shotList);

        this.sort = sort;
    }

    public void cleanList(){
        shotList.clear();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_shot, parent, false);
        return new ShotAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Shot shot = shotList.get(position);

        holder.shot = shot;
        holder.attachment.setText(String.valueOf(shot.getAttachmentsCount()));
        holder.views.setText(String.valueOf(shot.getViewsCount()));
        holder.comments.setText(String.valueOf(shot.getCommentsCount()));
        holder.likes.setText(String.valueOf(shot.getLikesCount()));

        Picasso.with(context)
                .load(shot.getImages().getHidpi())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return shotList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Shot shot;

        @BindView(R.id.shot_image)
        ImageView image;

        @BindView(R.id.shot_attachment)
        TextView attachment;

        @BindView(R.id.shot_views)
        TextView views;

        @BindView(R.id.shot_comments)
        TextView comments;

        @BindView(R.id.shot_likes)
        TextView likes;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Context context = itemView.getContext();

            Intent intent = new Intent(context, ShotDetailActivity.class);
            intent.putExtra(SHOT_ID, shot.getId());
            context.startActivity(intent);

        }
    }
}