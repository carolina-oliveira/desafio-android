package com.example.carol.desafio_android.controller;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.carol.desafio_android.R;
import com.example.carol.desafio_android.api.CustomApplication;
import com.example.carol.desafio_android.model.Shot;
import com.example.carol.desafio_android.ui.activity.CallbackActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShotController extends AppCompatActivity {
    private String LOG_TAG = ShotController.class.getSimpleName();

    public void makeShotListCall(String sort, int page, final CallbackActivity<List<Shot>> callbackActivity) {

        final CustomApplication application = (CustomApplication) getApplication();
        application.getApi()
                .getShotList("attachments", sort, page, 5)
                .enqueue(new Callback<List<Shot>>() {
                    @Override
                    public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {
                        if (response.isSuccessful()) {
                            callbackActivity.onCreateComplete(response.body());
                        } else if (response.code() == 404) {
                            callbackActivity.onCreateComplete(null);
                        } else {

                            Toast.makeText(ShotController.this, R.string.retrieve_shot_list_error_message, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, "Error: ");
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Shot>> call, Throwable t) {
                        Toast.makeText(ShotController.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        Log.e("MAIN", "Error", t);
                    }
                });
    }
}