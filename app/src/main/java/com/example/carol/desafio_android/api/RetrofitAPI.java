package com.example.carol.desafio_android.api;

import com.example.carol.desafio_android.model.Shot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitAPI {
    @GET("shots")
    @Headers({"Authorization: Bearer 04a28dcf3acfe434821537082cf948b6aa5a0ddb381c74b9419c515ca14f43d0"})
    Call<List<Shot>> getShotList(
            @Query("list") String list,
            @Query("sort") String sort,
            @Query("page") int page,
            @Query("per_page") int per_page

    );

    @GET("shots/{id}")
    @Headers({"Authorization: Bearer 04a28dcf3acfe434821537082cf948b6aa5a0ddb381c74b9419c515ca14f43d0"})
    Call<Shot> getShotItem(
            @Path("id") long id
    );
}
